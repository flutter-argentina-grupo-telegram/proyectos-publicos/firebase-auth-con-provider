import 'package:firebase_auth_provider_example/providers/auth.provider.dart';
import 'package:firebase_auth_provider_example/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthProvider>(
          builder: (_) => AuthProvider(),
          dispose: (_, provider) => provider.close(),
        ),
      ],
      child: MaterialApp(
        title: 'Firebase Auth Provider Example',
        initialRoute: INITIAL_ROUTE,
        onGenerateRoute: onGenerateRoute,
      ),
    );
  }
}
