import 'package:firebase_auth_provider_example/providers/auth.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text(
            'Login with google',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          color: Colors.red,
          onPressed: authProvider.signInWithGoogle,
        ),
      ),
    );
  }
}
