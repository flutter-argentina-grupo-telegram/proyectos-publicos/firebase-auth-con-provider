import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_provider_example/components/logout_button.component.dart';
import 'package:firebase_auth_provider_example/providers/auth.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AuthProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder<FirebaseUser>(
          stream: provider.userStream,
          builder: (context, snapshot) {
            return Text(snapshot.data?.email ?? '');
          },
        ),
        actions: <Widget>[LogoutButton()],
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Second page'),
          onPressed: () {
            Navigator.pushNamed(context, '/second');
          },
        ),
      ),
    );
  }
}
