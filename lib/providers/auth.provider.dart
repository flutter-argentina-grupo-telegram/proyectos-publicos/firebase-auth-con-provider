import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';

class AuthProvider {
  final _auth = FirebaseAuth.instance;
  final _google = GoogleSignIn();

  final _user = BehaviorSubject<FirebaseUser>();

  FirebaseUser get user => _user.value;
  set user(FirebaseUser newUser) => _user.sink.add(newUser);

  Stream<FirebaseUser> get userStream => _user.stream;

  AuthProvider() {
    _auth.onAuthStateChanged.listen((newUser) => user = newUser);
  }

  close() {
    _user.close();
  }

  Future<FirebaseUser> signInWithGoogle() async {
    try {
      final googleAccount = await _google.signIn();
      final googleAuth = await googleAccount.authentication;
      final credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final authResult = await _auth.signInWithCredential(credential);
      return authResult.user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool> signOut() async {
    try {
      await _google.signOut();
      await _auth.signOut();
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
