import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_provider_example/pages/home.page.dart';
import 'package:firebase_auth_provider_example/pages/login.page.dart';
import 'package:firebase_auth_provider_example/pages/second.page.dart';
import 'package:firebase_auth_provider_example/providers/auth.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const INITIAL_ROUTE = '/';

Route<Widget> onGenerateRoute(RouteSettings settings) {
  return MaterialPageRoute(
    settings: settings,
    builder: (context) {
      final authProvider = Provider.of<AuthProvider>(context);
      return StreamBuilder<FirebaseUser>(
        stream: authProvider.userStream,
        builder: (_, snapshot) {
          final user = snapshot.data;
          if (user == null) return LoginPage();
          switch (settings.name) {
            case '/':
              return HomePage();
            case '/second':
              return SecondPage();
            default:
              return Scaffold(
                body: Center(
                  child: Text('No implementado'),
                ),
              );
          }
        },
      );
    },
  );
}
