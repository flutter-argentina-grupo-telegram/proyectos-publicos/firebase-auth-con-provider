import 'package:firebase_auth_provider_example/providers/auth.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: authProvider.signOut,
    );
  }
}
